package com.ogi.putraranji.registrasiloginlaravelandroid.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.ogi.putraranji.registrasiloginlaravelandroid.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by ogi on 04/04/18.
 */

public class RetrofitBuilder {

    private static final String BASE_URL = "http://localhost/blog/public/api";

    private final static OkHttpClient client = buildClient();

    private final static Retrofit retrofit = buildRetrofit(client);


    private static OkHttpClient buildClient(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();

                        Request.Builder builder = request.newBuilder()
                                .addHeader("Accept", "application/json")
                                .addHeader("connection", "close");

                        request = builder.build();

                        return chain.proceed(request);
                    }
                });
        if(BuildConfig.DEBUG){
            builder.addNetworkInterceptor(new StethoInterceptor());
        }

        return builder.build();
    }

    private static Retrofit buildRetrofit(OkHttpClient client){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
    }

    //ApiService.class
    public static <T> T createService(Class<T> service){
        return retrofit.create(service);
    }
}
