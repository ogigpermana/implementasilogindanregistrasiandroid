package com.ogi.putraranji.registrasiloginlaravelandroid.network;

import com.ogi.putraranji.registrasiloginlaravelandroid.entities.AccessToken;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ogi on 04/04/18.
 */

public interface ApiService {

    @POST("register") //Untuk mengakses link http://domain.com/api
    @FormUrlEncoded
    Call<AccessToken> register(@Field("name")String name , @Field("email")String email, @Field("password")String password);

}
