package com.ogi.putraranji.registrasiloginlaravelandroid.entities;

import com.squareup.moshi.Json;

/**
 * Created by ogi on 04/04/18.
 */

public class AccessToken {
    @Json(name = "token_type")
    String tokenType;
    @Json(name = "expires_in")
    int expiresIn;
    @Json(name = "access_token")
    String accessToken;
    @Json(name = "refresh_token")
    String refreshToken;
}
